Контент для главной страницы. Тест.<br>
<code><?= __FILE__ ?></code>
<!--<br><?
/*=$name*/ ?><br>
<?
/*=$hi*/ ?><br>-->
<div class="container">
    <button class="btn btn-primary mb-2 mt-2" id="send">Отправить</button>
    <?php
    if (!empty($posts)): ?>
        <div class="row row-cols-1 row-cols-md-3 g-4">
            <?php
            foreach ($posts as $post): ?>
                <div class="col h-100">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><?= $post['name'] ?></h5>
                            <p class="card-text"><?= $post['desc'] ?></p>
                        </div>
                    </div>
                </div>
            <?php
            endforeach; ?>
        </div>
    <?php
    endif;
    ?>
</div>
<!--<script src="js/test.js"></script>-->
<script>
    $(function (){
        $('#send').click(function () {
            $.ajax({
                url: 'main/test',
                type: 'post',
                data: {'id': 7},
                success: function (res) {
                    console.log(res)
                },
                error: function () {
                    alert('Error!');
                }
            })
        })
    })
</script>