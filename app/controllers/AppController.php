<?php

namespace app\controllers;

use app\models\Main;
use vendor\core\base\Controller;

class AppController extends Controller
{
    public $menu;

    public function __construct($route)
    {
        parent::__construct($route);
        new Main(); // Пока создается любой объект, чтобы была связь с БД.
        $this->menu = \R::findAll('category');
    }
}