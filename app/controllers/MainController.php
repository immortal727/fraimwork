<?php namespace app\controllers;

use app\models\Main;
use vendor\core\base\View;

class MainController extends AppController
{
    public function indexAction()
    {
        // Если не нужно подключать шаблон
        //$this->layout = false;

        //\R::fancyDebug(true);
        $model = new Main();
        $posts = \R::findAll('posts');
        $menu = $this->menu;

        View::setMeta('Главная страница', 'Описание страницы', 'Ключевые фразы');
        //$meta=$this->meta;
        $this->set(compact('posts', 'menu'));
    }

    public function testAction()
    {
        $this->layout = 'test';
        $model = new Main();
        if ($this->isAjax()) {
            $post = \R::findOne('posts', 'id = ?', [$_POST['id']]);
            $this->loadView('_test', compact('post'));
            die();
        }

    }
}