<?php
// components - Содержит классы, которые будут подключаться в автозагрузке
$config = [
    'components' => [
        'cache' => 'vendor\libs\Cache',
        /*'label' => 'vendor\libs\Label',*/
    ],
];

return $config;