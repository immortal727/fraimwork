<?php namespace vendor\core\base;

class View
{
    /**
     * текущий маршрут и параметры (controller, action, params)
     * @var array
     */
    public array $route = [];

    /**
     * текущий вид
     * @var string
     */
    public string $view;

    /**
     * текущий шаблон
     * @var string
     */
    public string $layout;

    /**
     * Если в виде подключаются скрипты
     * @var array
     */
    public array $scripts = [];

    public static array $meta = ['title' => '', 'desc' => '', 'keywords' => ''];

    public function __construct($route, $layout = '', $view = '' )
    {
        $this->route =  $route;
        if ($layout === false) {
            $this->layout = false;
        } else {
            $this->layout = $layout ?: LAYOUT;
        }
        $this->view = $view;
    }

    /**
     * Рендеринг шаблона
     * @return void
     */
    public function render($vars) {
        if(is_array($vars)) {
            extract($vars);
        }
        // @todo В дальнейшем можно прикрутить шаблонизатор twig
        $file_view = APP . "/views/{$this->route['controller']}/{$this->view}.php";
        // Чтобы сразу не выводилось на экран, а попадало в буфер
        ob_start();
        if(is_file($file_view)) {
            require_once $file_view;
        } else {
            echo "<p>Не найден вид <b>$file_view</p>";
        }
        $content = ob_get_clean();
        if ($this->layout) {
            $file_layout = APP  . "/views/layouts/{$this->layout}.php";
            if (is_file($file_layout))
                // Перед подключением шаблона вида вырезаем скрипты.
                $content = $this->getScripts($content);
                if ($this->scripts[0]) {
                    // Убираем повторяющиеся значения из массива
                    $scripts = array_unique($this->scripts[0]);
                }
                require_once $file_layout;
        } else {
            echo "<p>Не найден шаблон <b>$file_layout</p>";
        }
    }

    /**
     * Вырезание скриптов, если они есть в контенте.
     * Требуется, чтоб перенести их перед закрывающим тэгом body.
     * @param $content
     * @return void
     */
    protected function  getScripts($content)
    {
        /**
         * Регулярное выражение для вырезания скриптов
         * /is в конце регулярного выражения делает поиск регистронезависимым (т.е. игнорирует регистр символов),
         * а модификатор "s" позволяет совпадать с символом новой строки (многострочный режим)
        */
        $pattern = "/<script.*?>.*?<\/script>/is";
        // Поиск шаблона $pattern в $content
        preg_match_all($pattern, $content, $this->scripts);
        if (!empty($this->scripts)) {
            $content = preg_replace($pattern, "", $content);
        }
        return $content;
    }

    /**
     * Устанавливает мета данные.
     *
     */
    public static function getMeta()
    {
        echo '<title>' . self::$meta['title'] . '</title>';
        echo '<meta name="description" content="' . self::$meta['desc'] . '">';
        echo '<meta name="keywords" content="' . self::$meta['keywords'] . '">';
    }

    /**
     * Устанавливает данные в мета.
     * @param string $title
     * @param string $desc
     * @param string $keywords
     *
     */
    public static function setMeta(string $title = '', string $desc = '', string $keywords = '')
    {
        self::$meta['title'] = $title;
        self::$meta['desc'] = $desc;
        self::$meta['keywords'] = $keywords;
    }
}