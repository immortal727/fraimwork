<?php namespace vendor\core\base;

abstract class Controller
{
    /**
    * текущий маршрут и параметры (controller, action, params)
    * @var array
    */
    public array $route = [];

    /**
    * текущий вид
    * @var string
     */
    public mixed $view;

    /**
     * Параметры для вида
     * @var array
     */
    public array $vars = [];

    /**
     * текущий шаблон
     */
    public $layout;

    public function __construct($route)
    {
        $this->route = $route;
        $this->view =  $route['action'];
        //include APP .  "/views/{$route['controller']}/{$this->view}.php";
    }

    public function getView()
    {
        $vObj = new View($this->route, $this->layout, $this->view);
        $vObj->render($this->vars);
    }

    /**
     * Переменная для передачи в вид.
     * @param $vars
     *
     */
    public function set($vars)
    {
        $this->vars = $vars;
    }

    public function isAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    /**
     * Передача параметров в вид
     * @param $view
     * @param array $params
     * @return void
     */
    public function loadView($view, array $params = []) {
        // Извлекаем данные из массива
        extract($params);
        // путь без папки, к примеру views/Main/index.php
        require_once APP . "/views/{$this->route['controller']}/{$view}.php";
    }

}