<?php namespace vendor\core;

class Registry
{
    use TSingleton;

    public static $objects = [];

    protected function __construct()
    {
        require_once ROOT . '/config/config.php';
        foreach ($config['components'] as $name => $component) {
            self::$objects[$name] = new $component;
        }
    }

    /**
     * Получение объекта класса.
     * @param $name
     * @return mixed|void
     */
    public function __get($name)
    {
        if (is_object(self::$objects[$name])) {
            return self::$objects[$name];
        }
    }

    /**
     * Создание объекта класса.
     * @param $name
     * @param $object
     * @return void
     */
    public function __set($name, $object)
    {
        if (!self::$objects[$name]) {
            self::$objects[$name] = new $object;
        }
    }

    /**
     * Вывод списка существующих классов.
     * @return void
     */
    public function getList() {
        echo '<pre>';
        var_dump(self::$objects);
        echo '</pre>';
    }
}