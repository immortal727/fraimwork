<?php namespace vendor\core;

trait TSingleton
{
    private static $instance;

    /**
     * Создание объектов класса.
     *
     */
    public static function instance()
    {
        if(self::$instance === null) {
            // Создание объекта класса.
            self::$instance = new self;
        }
        return self::$instance;
    }
}
