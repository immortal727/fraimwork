<?php

namespace vendor\core;

class Db
{
    use TSingleton;

    private $pdo;

    /**
     * Кол-во запросов
     * @var int
     */
    public static int $countSql = 0;
    /**
     * Массив, в котором сохраняются все запросы.
     * @var array
     */
    public static array $queries = [];

    private function __construct()
    {
        $db = require ROOT . '/config/config_db.php';
        require_once LIBS . '/rb-mysql.php';
        // установка режима вывода ошибок
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ];
        \R::setup($db['dsn'], $db['user'], $db['pass'], $options);
        // Запрет на изменение структуры таблиц.
        \R::freeze(true);
        // Для удобного дебага.
        /*\R::fancyDebug(true);*/
    }

    /**
     * Выполнение Sql запроса
     * @param $sql
     * @param array $params
     * @return bool
     */
    /*public function execute($sql, array $params = [])
    {
        self::$countSql++;
        self::$queries[] = $sql;
        $stmt = $this->pdo->prepare($sql);
        return $stmt->execute($params);
    }*/

    /**
     * Выбор данных из базы данных.
     * @param $sql
     * @param array $params
     * @return array|false
     */
    /*public function query($sql, array $params = []): bool|array
    {
        // Подготовка запроса.
        $stmt = $this->pdo->prepare($sql);
        self::$countSql++;
        self::$queries[] = $sql;
        // Обход массива с параметрами и подставлением значений.
        if ( !empty($params) ) {
            foreach ($params as $key => $value) {
                $stmt->bindValue(":$key", $value);
            }
        }

        // Выполнение запроса.
        $res = $stmt->execute($params);
        if($res) {
            return $stmt->fetchAll();
        }
        return [];
    }*/
}