<?php

namespace vendor\core;

class ErrorHandler
{
    public function __construct()
    {
        if(DEBUG != 'prod') {
            error_reporting(-1);
        } else {
            error_reporting(0);
        }
        // Устанавливаем пользовательский обработчик ошибок.
        set_error_handler([$this, 'errorHandler']);
        // Включаем буфферизацию вывода (вывод скрипта сохраняется во внутреннем буфере)
        ob_start();
        register_shutdown_function([$this, 'fatalErrorHandler']);
        set_exception_handler([$this, 'exeptionHandler']);
    }

    /**
     * Получение информации об ошибке
     * Можно расширить, используя https://habr.com/ru/articles/161483/
     *
     * @param $errno // Номер ошибки.
     * @param string $errmsg // Текст ошибки
     * @param string $errfile // Файл, в котором произошла ошибка.
     * @param string $errline // Строка, на которой произошла ошибка.
     * @return bool
     */
    public function errorHandler($errno, string $errmsg, string $errfile, string $errline){
        $this->logErrors($errmsg, $errfile, $errline);
        $this->displayError($errno, $errmsg, $errfile, $errline);
        return true;
    }


    /**
     * Для вывода фатальных ошибок, к примеру test().
     * @return void
     */
    public function fatalErrorHandler()
    {
        $error = error_get_last();
        // если была ошибка и она фатальна
        if (!empty($error) AND $error['type'] & ( E_ERROR | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR))
        {
            // очищаем буффер (не выводим стандартное сообщение об ошибке)
            $this->logErrors($error['message'], $error['file'], $error['line']);
            ob_end_clean();
            // Показ ошибки
            $this->displayError($error['type'], $error['message'], $error['file'], $error['line']);
        }
        else
        {
            // отправка (вывод) буфера и его отключение
            ob_end_flush();
        }
    }


    public function exeptionHandler($e)
    {
        $this->logErrors($e->getMessage(), $e->getFile(), $e->getLine());
        $this->displayError('Исключение', $e->getMessage(), $e->getFile(), $e->getLine(), $e->getCode());
    }


    /**
     * @param string $message Сообщение об ошибке.
     * @param string $file Файл, куда будет записываться лог.
     * @param string $line Указание линии, строки, в которой произошла ошибка.
     * @return void
     */
    protected function logErrors($message = '', $file = '', $line ='')
    {
        error_log("[" . date('Y-m-d H:i:s') . "] Текст ошибки: {$message} | Файл {$file} | Строка {$line}\n=========================\n", 3, ROOT . '/tmp/errors.log');

    }
    /**
     * В зависимости от константы DEBUG вывод ошибок.
     * @param $errno // Номер ошибки.
     * @param $errmsg // Текст ошибки
     * @param $errfile // Файл, в котором произошла ошибка.
     * @param $errline // Строка, на которой произошла ошибка.
     * @param $response // Код ошибки
     */
    private function displayError($errno, $errmsg, $errfile, $errline, $response = 500)
    {
        http_response_code($response);
        if($response == 404) {
            require_once WWW . '/errors/404.html';
            die;
        }
        if (DEBUG != 'prod') {
            require_once WWW . '/errors/dev.php';
        } else {
            require_once WWW . '/errors/prod.php';
        }
        die;
    }
}