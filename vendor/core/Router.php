<?php namespace vendor\core;

/**
 * Class Router
 *  Определение маршрутов
 */
class Router
{
    // Массив всех маршрутов
    private static array $routes = [];
    // Текущий маршрут
    private static array $route = [];

    /**
     * Добавление маршрута
     * @param $regexp
     * @param array $route
     * @return void
     */
    public static function add($regexp, array $route = [])
    {
        self::$routes[$regexp] = $route;
    }

    /**
     * Получение всех маршрутов
     * @return array
     */
    public static function getRoutes()
    {
        return self::$routes;
    }

    /**
     * Возвращает текущий маршрут
     * @return array
     */
    public static function getRoute()
    {
        return self::$route;
    }

    /**
     * Проверка соответствия маршруту и
     * формирование текущего роута
     * @param $url
     * @return bool
     */
    private static function matchRoute($url)
    {
        foreach (self::getRoutes() as $pattern => $route) {
            if (preg_match("#$pattern#i", $url, $matches)) {
                foreach ($matches as $k => $v) {
                    if (is_string($k)) {
                       $route[$k] = $v;
                    }
                }
                $route['controller'] = self::upperCamelCase($route['controller']) ;
                if (!isset($route['action'])) {
                    $route['action'] = 'index';
                }
                self::$route = $route;
                return true;
            }
        }
        return false;
    }

    /**
     * Перенаправление URL по корректному маршруту
     * @param string $url входящий URL
     * @return void
     */
    public static function dispatch($url)
    {
        $url = self::removeQueryString($url);
        if (self::matchRoute($url)) {
            $controller = 'app\controllers\\' . self::$route['controller'] . 'Controller';
            //debug(self::$route);
            if (class_exists($controller)) {
                $obj = new $controller(self::$route);
                // Методы приводится к стандарту и вызываться будут те методы, у которых в конце будет Action
                $method = self::lowerCamelCase(self::$route['action']) . 'Action';
                $action = method_exists($obj, $method) ? $method: 'indexAction';
                if (method_exists($obj, $action)) {
                    $obj->$action();
                    $obj->getView();
                }
                else {
                    //echo "Метод $action у $controller не найден";
                    throw new \Exception( "Метод $action у $controller не найден", 404);
                }
            } else {
                //echo "Контроллер <b>$controller</b> не найден";
                throw new \Exception( "Контроллер <b>$controller</b> не найден", 404);
            }
        } else {
            throw new \Exception( "Страница не найдена", 404);
        }
    }

    /**
     * Приведение к стандарту CamelCase для классов
     * @param $name
     * @return array|string|string[]
     */
    private static function upperCamelCase($name)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $name)));
    }

    /**
     * Привпедение методов класса к стандарту
     * @param $name
     * @return string
     */
    private static function lowerCamelCase($name)
    {
        return lcfirst(self::upperCamelCase($name));
    }

    /**
     * Обрезание параметров из строки запроса
     * @param $url
     * @return mixed
     */
    private static function removeQueryString($url)
    {
        if ($url) {
            $params = explode('?', $url);
            // Удаляем пробелы и другие символы из конца строки
            return rtrim($params[0], " \n\r\t\v\x00");
        }
    }
}
