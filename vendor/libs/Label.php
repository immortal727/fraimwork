<?php

namespace vendor\libs;

/**
 * Класс Label (метка) для использования в виде.
 * Нужна к примеру для подключения стилей или скриптов в нужном месте.
 *
 * @author Dmitriy Lugovskoy <kushiy@mail.ru>
 *
 * @version 1.0 - 25.12.2023
 */
class Label
{
    private static array $labels = [];

    /**
     * Вывод содержимого
     * @param $name
     * @param $content
     * @return void
     */
    public static function yield($name, $content)
    {
        self::$labels[$name] = $content;
    }

    /**
     * Отображение содержимого в метке.
     * @param $name
     * @return void
     */
    public static function section($name)
    {
        echo "<!--yield:{$name}-->";
        var_dump($name);
        if (isset(self::$labels[$name])) {
            echo self::$labels[$name];
        }
    }

}