<?php namespace vendor\libs;

class Cache
{
    public function __construct()
    {

    }

    /**
     * Добавление данных в кэш.
     * @param string $key // Ключ кэша
     * @param $data // Данные для кэша
     * @param int $second // Время кэширования
     * @return bool
     */
    public function set(string $key, $data, int $second = 3600)
    {
        $content['data'] = $data;
        $content['end_time'] = time() + $second;
        if (file_put_contents(CACHE . '/' . md5($key) . '.txt', serialize($content))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Достаем данные из кэша.
     * @param string $key
     * @return bool
     */
    public function get(string $key)
    {
        $file = CACHE . '/' . md5($key) . '.txt';
        if (file_exists($file)) {
            $content = unserialize(file_get_contents($file));
            // Если кэш актуалет
            if (time() <= $content['end_time']) {
                return $content['data'];
            }
            unlink($file);
        }
        return false;
    }

    /**
     * Удаление файла кэша по ключу.
     * @param string $key
     */
    public function delete($key) {
        {
            $file = CACHE . '/' . md5($key) . '.txt';
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
}