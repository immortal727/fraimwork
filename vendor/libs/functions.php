<?php

/**
 * Функция для удобного просмотра кода при дебаге
 * @param $arr
 * @return void
 */
function debug($arr)
{
    echo '<pre>' .  print_r($arr, true) . '</pre>';
}
