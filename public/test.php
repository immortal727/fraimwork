<?php
/*require_once 'rb-mysql.php';
$db = require '../config/config_db.php';
$options = [
    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
];
R::setup($db['dsn'], $db['user'], $db['pass'], $options);*/
// Запрет на изменение структуры таблиц.
//R::freeze(true);
// Для удобного дебага.
//R::fancyDebug(true);
//var_dump(R::testConnection());

//Create
/*$cat = R::dispense( 'category');
$cat->title = 'Категория 4';
$id = R::store($cat);*/

// Read
/*$cat = R::load('category', 1);
print_r($cat->title); // $cat['title'] */

// Update
/*$cat = R::load('category', 1);
$cat->title = 'Категория 1';
R::store($cat);*/

// Delete
/*$cat = R::load('category', 3);
R::trash($cat);*/

// Очищение всех записей из таблицы
// R::wipe('category');

// Все записи
//$cats = R::findAll('category');
/*$cats = R::findAll('category', 'id > ?', [2]);
$cats = R::findAll('category', 'title LIKE ?', ['%cat%']);*/
// Одна запись
//$cats = R::findOne('category', 'id = 2');
//echo '</pre>';
//print_r($cats);

