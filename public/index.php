<?php
use vendor\core\Router;
// URI, который был предоставлен для доступа к этой странице
$query = rtrim($_SERVER['REQUEST_URI'], '/');
define('WWW', __DIR__);
define('CORE', dirname(__DIR__) .  '/vendor/core');
define('LIBS', dirname(__DIR__) .  '/vendor/libs');
define('ROOT', dirname(__DIR__) );
define('APP', dirname(__DIR__) . '/app' );
define('CACHE', dirname(__DIR__) . '/tmp/cache');
define('LAYOUT', 'default');
define("DEBUG" , 'dev');

// Автозагрузка
spl_autoload_register(function ($class) {
    // Заменяем обратные \ прямыми / (нужно для Linux) и экранируем при помощи \\
    $file = ROOT  .  '/'  .  str_replace('\\' , '/',  $class)  .  '.php' ;
    if (is_file($file)) {
        require_once $file;
    }
});

new \vendor\core\App;

require_once '../vendor/libs/functions.php';

Router::add('^\/(?P<action>[a-z\-]+)?$', ['controller' => 'Posts' ]);
Router::add('^\/page/(?P<action>[a-z\-]+)\/(?P<alias>[a-z\-]+)$', ['controller' => 'Page' ]);

// defaults routs
Router::add('^$', ['controller'=>'Main', 'action'=>'index']);
// Запись вида ?P<controller> создает именнованные ключи
Router::add('^\/(?P<controller>[a-z\-]+)\/(?P<action>[a-z\-]+)$');
//debug(Router::getRoutes());
Router::dispatch($query);

